import Entity from './Entity';

export default class StarWarsUniverse {
	constructor(){
		this.entities = [];
		
	}
	
	async init(){
		
		var categories = [];
		
		//Extract all api addresses with different categories
		const url = "https://swapi.boom.dev/api/";
		const rootResource = await fetch(url)
			.then(response => response.json())
			.then(data => {
				categories = Object.keys(data);
			});
			
		//Loop for each url to be fetched
		for(var category of categories){
			// There are still pages with data when the data.next property is different from null
			var availablePages = true;
			//Page number
			var number = 1;
			
			var entity = new Entity();
			
			while(availablePages){
				//Fetch data from each urls based on the root resource
				await fetch(`https://swapi.boom.dev/api/${category}/?page=${number}?format=json`)
				.then(response => response.json())
				.then(data => {
						if(data.next === null)
							availablePages = false; // no more pages with available
						
						//Store each name and data from one category of the API in separate entity
						entity.name = category;
						entity.data.count = data.count;
						entity.data.results.push(...data.results);
						
						number++;
				});
			}
			//Store each entity in the array of entities
			this.entities.push(entity);	
		}
	}
}
